let nav = document.getElementsByTagName('NAV');
let navButton = document.getElementById('navButton');
let menuButton = document.getElementById('menuButton');
let closeButton = document.getElementById('closeButton');
let menuLinks = document.getElementById('menuLinks');
let mainCover =	document.getElementById('mainCover');

function viewMenuList() {
	if(menuLinks.style.display === 'none') {
		menuButton.style.display = 'none';
		closeButton.style.display = 'inline';
		menuLinks.style.display = 'flex';
		mainCover.style.display = 'block';
	} else {
		menuButton.style.display = 'inline';
		closeButton.style.display = 'none';
		menuLinks.style.display = 'none';
		mainCover.style.display = 'none';
	}
}

document.body.onresize = () => {
	if(nav[0].clientWidth > 800) {
		menuLinks.style.display = 'flex';
		menuButton.style.display = 'none';
		closeButton.style.display = 'inline';
		mainCover.style.display = 'none';
	} else {
		mainCover.style.display = 'none';
		menuLinks.style.display = 'none';
		menuButton.style.display = 'inline';
	}
}

navButton.onclick = () => {
	viewMenuList();
}